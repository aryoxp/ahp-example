using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AnalyticalHierarchyProcess
{
    public partial class FormAHP : Form
    {
        private String[] Parameters = new String[] { };
        private String[] Cars = new String[] { };

        private Matrix EigenVectorCriteria;
        private List<Matrix> EigenVectorAlternative = new List<Matrix>();

        public FormAHP()
        {
            InitializeComponent();
            InitialValue();
        }

        #region Initial Value
        private void InitialValue()
        {
            Parameters = new String[] { "Style", "Reliability", "Fuel Economy", "Prestige", "After Sales Service" };
            Cars = new String[] { "Honda Civic Si", "Nissan Elgrand", "Ford Escort", "Mazda Miata", "BMW 325i" };

            listParam.Items.Clear();
            listParam.Items.AddRange( this.Parameters );
            listParam.SelectedIndex = 0;
            listParamOrder.Items.Clear();

            listObject.Items.Clear();
            listObjectOrder.Items.Clear();

            gbParameter.Enabled = true;
            gbObject.Enabled = false;

            tbOutput.Clear();



            EigenVectorCriteria = null;
            EigenVectorAlternative.Clear();
        }
        #endregion

        #region Debug Function
        public void Debug(Matrix m, String name)
        {
            tbOutput.SelectionFont = new Font("Tahoma", (float)8, FontStyle.Bold);
            tbOutput.SelectedText += " " + name + "\r\n";
            tbOutput.SelectionFont = new Font("Tahoma", (float)8, FontStyle.Regular);
            tbOutput.SelectedText += m.ToString() + "\r\n\r\n";

        }
        #endregion



        private void btClear_Click(object sender, EventArgs e)
        {
            tbOutput.Clear();
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            if (listParam.SelectedIndex != -1)
            {
                Int32 index = listParam.SelectedIndex;
                listParamOrder.Items.Add(listParam.SelectedItem);
                listParamOrder.SelectedIndex = listParamOrder.Items.Count - 1;
                listParam.Items.Remove(listParam.SelectedItem);

                if (listParam.Items.Count > 0) listParam.SelectedIndex = 0;
                if (listParam.Items.Count >= index) listParam.SelectedIndex = index - 1;
                if (listParam.SelectedIndex < 0 && listParam.Items.Count > 0) listParam.SelectedIndex = 0;
            }
        }

        private void btReset_Click(object sender, EventArgs e)
        {
            InitialValue();
        }

        private void btRemove_Click(object sender, EventArgs e)
        {
            if (listParamOrder.SelectedIndex != -1)
            {
                Int32 index = listParamOrder.SelectedIndex;
                listParam.Items.Add(listParamOrder.SelectedItem);
                listParam.SelectedIndex = listParam.Items.Count - 1;
                listParamOrder.Items.Remove(listParamOrder.SelectedItem);

                if (listParamOrder.Items.Count > 0) listParamOrder.SelectedIndex = 0;
                if (listParamOrder.Items.Count >= index) listParamOrder.SelectedIndex = index - 1;
                if (listParamOrder.SelectedIndex < 0 && listParamOrder.Items.Count > 0) listParamOrder.SelectedIndex = 0;
            }
        }

        private void btProcess_Click(object sender, EventArgs e)
        {
            try
            {
                tbOutput.Clear();
                if (listParamOrder.Items.Count < 2) throw new Exception("Minimal ada dua parameter yang dipilih");

                Int32[] LikertScale = new Int32[] { 9, 7, 5, 3, 1 };

                //remove unused params
                Parameters = new String[] { "Style", "Reliability", "Fuel Economy", "Prestige", "After Sales Service" };
                List<String> temp = new List<String>();
                foreach (String Parameter in this.Parameters)
                {
                    if (listParamOrder.Items.IndexOf(Parameter) != -1) temp.Add(Parameter);
                }
                this.Parameters = temp.ToArray();

                //remove unused scale
                Array.Copy(LikertScale, (5 - Parameters.Length), LikertScale, 0, Parameters.Length);



                List<Double> ValueOrder = new List<Double>();
                Int32 y = 0;
                foreach (String Parameter in this.Parameters)
                {
                    Int32 x = listParamOrder.Items.IndexOf(Parameter);
                    if (x != -1)
                    {
                        ValueOrder.Add(LikertScale[x]);
                    }
                    y++;
                }

                Matrix Criteria = new Matrix(listParamOrder.Items.Count, listParamOrder.Items.Count);
                for (Int32 i = 0; i < listParamOrder.Items.Count; i++)
                {
                    for (Int32 j = 0; j < listParamOrder.Items.Count; j++)
                    {
                        Criteria[i, j] = (Double)ValueOrder.ToArray()[i] / (Double)ValueOrder.ToArray()[j];
                    }
                }
                Debug(Criteria, " Criteria:");

                Matrix CriteriaSq = Matrix.Multiply(Criteria, Criteria);
                Debug(CriteriaSq, " Criteria Square");

                Matrix CriteriaSqRowSum = RowSum(CriteriaSq);
                Debug(CriteriaSqRowSum, " Criteria Row Sum");

                Matrix CriteriaSqRowSumNorm = Normalize(CriteriaSqRowSum);
                Debug(CriteriaSqRowSumNorm, " Criteria Row Sum Normalized");

                // First Iteration
                Int32 iterationCount = 1;
                Boolean Complete = false;
                Matrix CriteriaSqRowSumNorm1;
                do
                {
                    Matrix CriteriaSq1 = Matrix.Multiply(CriteriaSq, CriteriaSq);
                    Debug(CriteriaSq1, " Criteria Square: " + iterationCount + " iteration");

                    Matrix CriteriaSqRowSum1 = RowSum(CriteriaSq1);
                    Debug(CriteriaSqRowSum1, " Criteria Row Sum: " + iterationCount + " iteration");

                    CriteriaSqRowSumNorm1 = Normalize(CriteriaSqRowSum1);
                    Debug(CriteriaSqRowSumNorm1, " Criteria Row Sum Normalized: " + iterationCount + " iteration");

                    Matrix check = Matrix.Subtract(CriteriaSqRowSumNorm1, CriteriaSqRowSumNorm);
                    Debug(check, "Iteration Check Matrix");

                    for (Int32 i = 0; i < check.NoRows; i++)
                    {
                        if (check[i, 0] > -0.0001 && check[i, 0] < 0.0001) Complete = true;
                        if (Complete) break;
                    }

                    CriteriaSq = CriteriaSq1;

                    iterationCount++;

                } while (!Complete && iterationCount < 5);

                this.EigenVectorCriteria = CriteriaSqRowSumNorm1;
                Debug(EigenVectorCriteria, "Eigen Vector Criteria (EVC)");

                AlternativeProcessInit(Parameters[0]);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            

        }

        private void AlternativeProcessInit(String Parameter)
        {
            gbParameter.Enabled = false;
            gbObject.Enabled = true;
            btProsesAlt.Enabled = true;

            tParam.Text = Parameter;

            String CriteriaOutput = tbOutput.Text;

            listObject.Items.Clear();
            listObject.Items.AddRange(this.Cars);
            listObjectOrder.Items.Clear();
            if (listObject.Items.Count > 0) listObject.SelectedIndex = 0;

            
        }

        #region Normalize Eigen Values
        private static Matrix Normalize(Matrix CriteriaRowSum)
        {
            Matrix CriteriaRowSumNorm = new Matrix(CriteriaRowSum.NoRows, CriteriaRowSum.NoCols);
            Double Sum = 0;
            for (Int32 row = 0; row < CriteriaRowSum.NoRows; row++) Sum += CriteriaRowSum[row, 0];
            for (Int32 row = 0; row < CriteriaRowSum.NoRows; row++)
            {
                CriteriaRowSumNorm[row, 0] = CriteriaRowSum[row, 0] / Sum;
            }
            return CriteriaRowSumNorm;
        }
        #endregion

        #region Row Sum
        private static Matrix RowSum(Matrix Criteria2)
        {
            Matrix CriteriaRowSum = new Matrix(Criteria2.NoRows, 1);
            for (Int32 row = 0; row < Criteria2.NoRows; row++)
            {
                for (Int32 col = 0; col < Criteria2.NoCols; col++)
                {
                    CriteriaRowSum[row, 0] += Criteria2[row, col];
                }
            }
            return CriteriaRowSum;
        }
        #endregion

        private void btAdd2_Click(object sender, EventArgs e)
        {
            if (listObject.SelectedIndex != -1)
            {
                Int32 index = listObject.SelectedIndex;
                listObjectOrder.Items.Add(listObject.SelectedItem);
                listObjectOrder.SelectedIndex = listObjectOrder.Items.Count - 1;
                listObject.Items.Remove(listObject.SelectedItem);

                if (listObject.Items.Count > 0) listObject.SelectedIndex = 0;
                if (listObject.Items.Count >= index) listObject.SelectedIndex = index - 1;
                if (listObject.SelectedIndex < 0 && listObject.Items.Count > 0) listObject.SelectedIndex = 0;
            }
        }

        private void btRemove2_Click(object sender, EventArgs e)
        {
            if (listObjectOrder.SelectedIndex != -1)
            {
                Int32 index = listObjectOrder.SelectedIndex;
                listObject.Items.Add(listObjectOrder.SelectedItem);
                listObject.SelectedIndex = listObject.Items.Count - 1;
                listObjectOrder.Items.Remove(listObjectOrder.SelectedItem);

                if (listObjectOrder.Items.Count > 0) listObjectOrder.SelectedIndex = 0;
                if (listObjectOrder.Items.Count >= index) listObjectOrder.SelectedIndex = index - 1;
                if (listObjectOrder.SelectedIndex < 0 && listObjectOrder.Items.Count > 0) listObjectOrder.SelectedIndex = 0;
            }
        }

        private void btProsesAlt_Click(object sender, EventArgs e)
        {
            if (listObject.Items.Count == 0 && listObjectOrder.Items.Count > 0)
            {
                // Mapping Alternative Order to Likert Scale
                Int32[] LikertScale = new Int32[] { 9, 7, 5, 3, 1 };
                List<Double> ValueOrder = new List<Double>();
                Int32 y = 0;
                foreach (String Car in this.Cars)
                {
                    Int32 x = listObjectOrder.Items.IndexOf(Car);
                    if (x != -1)
                    {
                        ValueOrder.Add(LikertScale[x]);
                    }
                    y++;
                }

                // Mapping Alternative Matrix
                Matrix Alternative = new Matrix(listObjectOrder.Items.Count, listObjectOrder.Items.Count);
                for (Int32 i = 0; i < listObjectOrder.Items.Count; i++)
                {
                    for (Int32 j = 0; j < listObjectOrder.Items.Count; j++)
                    {
                        Alternative[i, j] = (Double)ValueOrder.ToArray()[i] / (Double)ValueOrder.ToArray()[j];
                    }
                }
                Debug(Alternative, " Alternative:");

                Matrix AlternativeSq = Matrix.Multiply(Alternative, Alternative);
                Debug(AlternativeSq, " Alternative Square");

                Matrix AlternativeSqRowSum = RowSum(AlternativeSq);
                Debug(AlternativeSqRowSum, " Alternative Row Sum");

                Matrix AlternativeSqRowSumNorm = Normalize(AlternativeSqRowSum);
                Debug(AlternativeSqRowSumNorm, " Alternative Row Sum Normalized");

                // First Iteration
                Int32 iterationCount = 1;
                Boolean Complete = false;
                Matrix AlternativeSqRowSumNorm1;
                do
                {
                    Matrix AlternativeSq1 = Matrix.Multiply(AlternativeSq, AlternativeSq);
                    Debug(AlternativeSq1, " Alternative Square: " + iterationCount + " iteration");

                    Matrix AlternativeSqRowSum1 = RowSum(AlternativeSq1);
                    Debug(AlternativeSqRowSum1, " Alternative Row Sum: " + iterationCount + " iteration");

                    AlternativeSqRowSumNorm1 = Normalize(AlternativeSqRowSum1);
                    Debug(AlternativeSqRowSumNorm1, " Alternative Row Sum Normalized: " + iterationCount + " iteration");

                    Matrix check = Matrix.Subtract(AlternativeSqRowSumNorm1, AlternativeSqRowSumNorm);
                    Debug(check, "Iteration Check Matrix");

                    for (Int32 i = 0; i < check.NoRows; i++)
                    {
                        if (check[i, 0] > -0.0001 && check[i, 0] < 0.0001) Complete = true;
                        if (Complete) break;
                    }

                    AlternativeSq = AlternativeSq1;

                    iterationCount++;

                } while (!Complete && iterationCount < 5);

                EigenVectorAlternative.Add(AlternativeSqRowSumNorm1);
                Debug(EigenVectorAlternative[EigenVectorAlternative.Count-1], "Eigen Vector Alternative "+Parameters[EigenVectorAlternative.Count-1]);

                if (EigenVectorAlternative.Count == Parameters.Length)
                {
                    btProsesAlt.Enabled = false;

                    Matrix ParameterAlternative = new Matrix(Cars.Length, Parameters.Length);
                    for (Int32 j = 0; j < Parameters.Length; j++)
                    {
                        for (Int32 i = 0; i < Cars.Length; i++)
                        {
                            Matrix Eigen = EigenVectorAlternative[j];
                            ParameterAlternative[i, j] = Eigen[i, 0];
                        }
                    }
                    Debug(ParameterAlternative, "Parameter Alternative Matrix (PA)");

                    Matrix FinalPriority = Matrix.Multiply(ParameterAlternative, EigenVectorCriteria);
                    Debug(FinalPriority, "Final Priority Matrix\r\n (PA.EVC)");

                    Double Max = 0;
                    Int32 MaxIndex = 0;
                    for (Int32 i = 0; i < FinalPriority.NoRows; i++)
                    {
                        if (FinalPriority[i, 0] > Max)
                        {
                            Max = FinalPriority[i, 0];
                            MaxIndex = i;
                        }
                    }

                    listObjectOrder.Items.Clear();
                    gbObject.Enabled = false;

                    MessageBox.Show("Mobil " + Cars[MaxIndex] + " sesuai dengan Anda!", "Final Result", MessageBoxButtons.OK, MessageBoxIcon.Information);



                }
                else
                {
                    AlternativeProcessInit(Parameters[EigenVectorAlternative.Count]);
                }

            }
            else
            {
                MessageBox.Show("Semua alternatif harus dipilih, urutan sesuai pioritas Anda", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

    }
}