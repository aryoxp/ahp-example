namespace AnalyticalHierarchyProcess
{
    partial class FormAHP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btClear = new System.Windows.Forms.Button();
            this.tbOutput = new System.Windows.Forms.RichTextBox();
            this.btReset = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.listObject = new System.Windows.Forms.ListBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.listObjectOrder = new System.Windows.Forms.ListBox();
            this.gbObject = new System.Windows.Forms.GroupBox();
            this.tParam = new System.Windows.Forms.Label();
            this.btRemove2 = new System.Windows.Forms.Button();
            this.btAdd2 = new System.Windows.Forms.Button();
            this.gbParameter = new System.Windows.Forms.GroupBox();
            this.btProcess = new System.Windows.Forms.Button();
            this.btRemove = new System.Windows.Forms.Button();
            this.btAdd = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.listParamOrder = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.listParam = new System.Windows.Forms.ListBox();
            this.btProsesAlt = new System.Windows.Forms.Button();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.gbObject.SuspendLayout();
            this.gbParameter.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.Location = new System.Drawing.Point(13, 13);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(310, 29);
            label1.TabIndex = 2;
            label1.Text = "Analytical Hierarchy Process";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label2.Location = new System.Drawing.Point(9, 21);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(96, 18);
            label2.TabIndex = 16;
            label2.Text = "Parameter";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.btClear);
            this.groupBox1.Controls.Add(this.tbOutput);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(482, 79);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(247, 461);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Debug Message";
            // 
            // btClear
            // 
            this.btClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btClear.Location = new System.Drawing.Point(166, 432);
            this.btClear.Name = "btClear";
            this.btClear.Size = new System.Drawing.Size(75, 23);
            this.btClear.TabIndex = 1;
            this.btClear.Text = "Clear";
            this.btClear.UseVisualStyleBackColor = true;
            this.btClear.Click += new System.EventHandler(this.btClear_Click);
            // 
            // tbOutput
            // 
            this.tbOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbOutput.Location = new System.Drawing.Point(6, 20);
            this.tbOutput.Name = "tbOutput";
            this.tbOutput.Size = new System.Drawing.Size(235, 406);
            this.tbOutput.TabIndex = 0;
            this.tbOutput.Text = "";
            this.tbOutput.WordWrap = false;
            // 
            // btReset
            // 
            this.btReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btReset.Location = new System.Drawing.Point(657, 30);
            this.btReset.Name = "btReset";
            this.btReset.Size = new System.Drawing.Size(66, 23);
            this.btReset.TabIndex = 8;
            this.btReset.Text = "Reset All";
            this.btReset.UseVisualStyleBackColor = true;
            this.btReset.Click += new System.EventHandler(this.btReset_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.listObject);
            this.groupBox5.Location = new System.Drawing.Point(6, 47);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(187, 177);
            this.groupBox5.TabIndex = 11;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Daftar Alternatif";
            // 
            // listObject
            // 
            this.listObject.FormattingEnabled = true;
            this.listObject.Location = new System.Drawing.Point(6, 20);
            this.listObject.Name = "listObject";
            this.listObject.Size = new System.Drawing.Size(174, 147);
            this.listObject.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.listObjectOrder);
            this.groupBox4.Location = new System.Drawing.Point(271, 47);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(187, 177);
            this.groupBox4.TabIndex = 12;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Daftar Prioritas Alternatif";
            // 
            // listObjectOrder
            // 
            this.listObjectOrder.FormattingEnabled = true;
            this.listObjectOrder.Location = new System.Drawing.Point(6, 21);
            this.listObjectOrder.Name = "listObjectOrder";
            this.listObjectOrder.Size = new System.Drawing.Size(175, 147);
            this.listObjectOrder.TabIndex = 1;
            // 
            // gbObject
            // 
            this.gbObject.Controls.Add(this.btProsesAlt);
            this.gbObject.Controls.Add(this.tParam);
            this.gbObject.Controls.Add(label2);
            this.gbObject.Controls.Add(this.btRemove2);
            this.gbObject.Controls.Add(this.btAdd2);
            this.gbObject.Controls.Add(this.groupBox4);
            this.gbObject.Controls.Add(this.groupBox5);
            this.gbObject.Location = new System.Drawing.Point(12, 275);
            this.gbObject.Name = "gbObject";
            this.gbObject.Size = new System.Drawing.Size(464, 265);
            this.gbObject.TabIndex = 14;
            this.gbObject.TabStop = false;
            this.gbObject.Text = "Alternatif Setiap Kriteria";
            // 
            // tParam
            // 
            this.tParam.AutoSize = true;
            this.tParam.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tParam.Location = new System.Drawing.Point(103, 21);
            this.tParam.Name = "tParam";
            this.tParam.Size = new System.Drawing.Size(96, 18);
            this.tParam.TabIndex = 17;
            this.tParam.Text = "Parameter";
            // 
            // btRemove2
            // 
            this.btRemove2.Location = new System.Drawing.Point(199, 147);
            this.btRemove2.Name = "btRemove2";
            this.btRemove2.Size = new System.Drawing.Size(66, 23);
            this.btRemove2.TabIndex = 15;
            this.btRemove2.Text = "Remove";
            this.btRemove2.UseVisualStyleBackColor = true;
            this.btRemove2.Click += new System.EventHandler(this.btRemove2_Click);
            // 
            // btAdd2
            // 
            this.btAdd2.Location = new System.Drawing.Point(199, 118);
            this.btAdd2.Name = "btAdd2";
            this.btAdd2.Size = new System.Drawing.Size(66, 23);
            this.btAdd2.TabIndex = 14;
            this.btAdd2.Text = "Add";
            this.btAdd2.UseVisualStyleBackColor = true;
            this.btAdd2.Click += new System.EventHandler(this.btAdd2_Click);
            // 
            // gbParameter
            // 
            this.gbParameter.Controls.Add(this.btProcess);
            this.gbParameter.Controls.Add(this.btRemove);
            this.gbParameter.Controls.Add(this.btAdd);
            this.gbParameter.Controls.Add(this.groupBox3);
            this.gbParameter.Controls.Add(this.groupBox2);
            this.gbParameter.Location = new System.Drawing.Point(12, 79);
            this.gbParameter.Name = "gbParameter";
            this.gbParameter.Size = new System.Drawing.Size(464, 190);
            this.gbParameter.TabIndex = 20;
            this.gbParameter.TabStop = false;
            this.gbParameter.Text = "Parameter Kriteria";
            // 
            // btProcess
            // 
            this.btProcess.Location = new System.Drawing.Point(6, 161);
            this.btProcess.Name = "btProcess";
            this.btProcess.Size = new System.Drawing.Size(452, 23);
            this.btProcess.TabIndex = 14;
            this.btProcess.Text = "Proses Prioritas Parameter";
            this.btProcess.UseVisualStyleBackColor = true;
            this.btProcess.Click += new System.EventHandler(this.btProcess_Click);
            // 
            // btRemove
            // 
            this.btRemove.Location = new System.Drawing.Point(205, 94);
            this.btRemove.Name = "btRemove";
            this.btRemove.Size = new System.Drawing.Size(66, 23);
            this.btRemove.TabIndex = 13;
            this.btRemove.Text = "Remove";
            this.btRemove.UseVisualStyleBackColor = true;
            this.btRemove.Click += new System.EventHandler(this.btRemove_Click);
            // 
            // btAdd
            // 
            this.btAdd.Location = new System.Drawing.Point(205, 65);
            this.btAdd.Name = "btAdd";
            this.btAdd.Size = new System.Drawing.Size(66, 23);
            this.btAdd.TabIndex = 12;
            this.btAdd.Text = "Add";
            this.btAdd.UseVisualStyleBackColor = true;
            this.btAdd.Click += new System.EventHandler(this.btAdd_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.listParamOrder);
            this.groupBox3.Location = new System.Drawing.Point(277, 20);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(181, 135);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Parameter List Priority";
            // 
            // listParamOrder
            // 
            this.listParamOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.listParamOrder.FormattingEnabled = true;
            this.listParamOrder.Location = new System.Drawing.Point(6, 21);
            this.listParamOrder.Name = "listParamOrder";
            this.listParamOrder.Size = new System.Drawing.Size(169, 108);
            this.listParamOrder.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.listParam);
            this.groupBox2.Location = new System.Drawing.Point(6, 20);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(193, 135);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Parameter List";
            // 
            // listParam
            // 
            this.listParam.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.listParam.FormattingEnabled = true;
            this.listParam.Location = new System.Drawing.Point(6, 20);
            this.listParam.Name = "listParam";
            this.listParam.Size = new System.Drawing.Size(180, 108);
            this.listParam.TabIndex = 0;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(15, 42);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(174, 13);
            label3.TabIndex = 21;
            label3.Text = "Aryo Pinandito / NRP. 9109205506";
            // 
            // btProsesAlt
            // 
            this.btProsesAlt.Location = new System.Drawing.Point(6, 230);
            this.btProsesAlt.Name = "btProsesAlt";
            this.btProsesAlt.Size = new System.Drawing.Size(452, 23);
            this.btProsesAlt.TabIndex = 20;
            this.btProsesAlt.Text = "Proses Alternatif untuk Kriteria";
            this.btProsesAlt.UseVisualStyleBackColor = true;
            this.btProsesAlt.Click += new System.EventHandler(this.btProsesAlt_Click);
            // 
            // FormAHP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(741, 552);
            this.Controls.Add(label3);
            this.Controls.Add(this.gbParameter);
            this.Controls.Add(this.gbObject);
            this.Controls.Add(this.btReset);
            this.Controls.Add(label1);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FormAHP";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Analytical Hierarchy Process";
            this.groupBox1.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.gbObject.ResumeLayout(false);
            this.gbObject.PerformLayout();
            this.gbParameter.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RichTextBox tbOutput;
        private System.Windows.Forms.Button btClear;
        private System.Windows.Forms.Button btReset;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ListBox listObject;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ListBox listObjectOrder;
        private System.Windows.Forms.GroupBox gbObject;
        private System.Windows.Forms.Button btRemove2;
        private System.Windows.Forms.Button btAdd2;
        private System.Windows.Forms.Label tParam;
        private System.Windows.Forms.GroupBox gbParameter;
        private System.Windows.Forms.Button btProcess;
        private System.Windows.Forms.Button btRemove;
        private System.Windows.Forms.Button btAdd;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListBox listParamOrder;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox listParam;
        private System.Windows.Forms.Button btProsesAlt;
    }
}

